#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
se declara una lista vacía, la cual será rellenada con * para armar
un triángulo dentro de una matriz, cuya altura será pedida al usuario
"""
lista = []
altura = int(input("Ingrese el tamaño de la lista: "))
tope = (altura * 2)

"""
se inicia el ciclo for que llena la matriz, para así formar un
triángulo, se usa lista.append para reemplazar un valor en la lista, en 
este caso, con "*" y "", paraa formar el triangulo
"""
for i in range(altura):
	lista.append([])
	for j in range(tope):
		if ((j > i) and (j < (tope - (i + 1)))):
			lista[i].append(' ')
		else:
			lista[i].append('*')
			
"""
este ciclo forma la matriz, con los valores del ciclo anterior 
("*" y " ")
"""
for i in range(altura):
	for j in range(tope):
		print (lista[i][j],end=" ")
	print("\n")
