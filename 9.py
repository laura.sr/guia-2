#!/usr/bin/env python
# -*- coding: utf-8 -*-
lista = []
tamano = int(input("Ingrese el valor de la matriz: "))

for i in range(tamano):
	lista.append([])
	for j in range(tamano):
		if(i == j):
			lista[i].append("1")
		else:
			lista[i].append("0")
				
for i in range(tamano):
	for j in range(tamano):
		print (lista[i][j], end = " ")
	print("\n")
