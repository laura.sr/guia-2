#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random 
#se declara la lista con los valores que contendrá
lista = ["Hola", "Bienvenidos", "Adios", "Buendia"]
largo = random.randint(4, 10)
lista_largo = []
lista_al_reves = []

"""
la función largo_lista permite, a través de un ciclo for, obtener 
palabras aleatorias de la lista, dependiendo del largo que ésta tiene
"""
def largo_lista(largo, lista_largo, lista):
	for i in range(largo):
		lista_largo.append(lista[random.randint(0,3)])
	return lista_largo

"""
la función lista_invertida permite obtener la lista aleatoria, pero
al revés, a través de un ciclo for utilizando "reversed"
"""
def lista_invertida(lista_al_reves, lista_largo):
	for i in  reversed(lista_largo):
		lista_al_reves.append(i)
	return lista_al_reves

"""
se llama a las funciones declaradas con anterioridad, para luego 
imprimir las listas
"""
largo_lista(largo, lista_largo, lista)
lista_invertida(lista_al_reves, lista_largo)
print(lista_largo)	
print(lista_al_reves)
	
	
